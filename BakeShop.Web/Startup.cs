﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BakeShop.Web.Startup))]
namespace BakeShop.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
